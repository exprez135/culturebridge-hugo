---
title: The CultureBridge Manual
tableOfContents: false
---

# CultureBridge Tulsa

Welcome to CultureBridge Tulsa!

CultureBridge Tulsa is a program of Cohort XI –
[Youth Philanthropy
Initiative](/info/youth-philanthropy-initiative) in Tulsa,
Oklahoma. Welcome to our Manual! This site will give you all of the
information and resources you need to explore CultureBridge, replicate
the program, and run your own immigration simulation!

## Our Mission {#our_mission}

Through participation in an interactive simulation, we promote a greater
understanding of the immigrant experience to inspire the people of Tulsa
to empathize with immigrants and to unite against cultural and political
barriers.

## What is CultureBridge Tulsa? {#what_is_culturebridge_tulsa}

Founded by local high school students of diverse backgrounds,
CultureBridge offers a brand-new perspective on immigrant life in the
United States of America. As our team of 28 students came together, we
recognized a lack of empathy for the immigrant experience amongst
non-immigrant communities. How could we bridge this gap of
understanding? After collaborating with the [New Tulsans
Initiative](/info/new-tulsans-initiative), communicating with
community organizations, and researching best practices, we created the
CultureBridge Immigrant Simulation. By taking on the role of an
immigrant in contemporary America, participants have the opportunity to
experience both the joys and the hardships of immigrating to and living
in our country.

## How can I get started? {#how_can_i_get_started}

Explore the Contents section below. Try reading our [Executive
Summary](Executive_Summary "wikilink") and [PR
Packet](PR_Packet "wikilink") to get an idea of who we are. Visit our
website at <https://culturebridgetulsa.com> to see our
[Video](/supplements/video).

Then, jump into the manual itself. We split the manual into
[Modules](/modules) (sections of the simulation itself) and
[Supplements](/supplements) (additional materials like the [PR
Packet](PR_Packet "wikilink") or the [Stories](/supplements/stories)).
