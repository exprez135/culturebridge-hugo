---
title: "Youth Philanthropy Initiative"
weight: 1
bookFlatSection: false
bookToc: false
bookHidden: false
bookCollapseSection: false
bookComments: true
---

# Youth Philanthropy Initiative

| **Name** | Youth Philanthropy Initiative |
| --- | --- |
| **Website** | <https://ypitulsa.org> |
| **Funding** | Charles and Lynn Schustermann Family Foundation |
| **Founded** | 2007 |

[Youth Philanthropy
Initiative](https://ypitulsa.org) equips teens with
the necessary tools and understanding to become social entrepreneurs.
Teens in YPI are socially and culturally aware, building their projects
around issues or problems they believe are pertinent in the Tulsa
community. All YPI projects are built to be sustainable through
research, organization, and long-term planning, giving students hands-on
experiences to use in college and beyond. YPI students are involved in
leadership, athletics, speech and debate, art, programming, but they all
come together with the same passion to make tangible change in their
communities.
