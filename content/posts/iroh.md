---
title: "Iroh"
date: 2020-04-21T16:27:22-05:00
description: Wisdom from Iroh
tags: ["wisdom"]
categories: ["advice"]
menu: main
bookHidden: true
---
Leaves from the vine

Falling so slow

Like fragile, tiny shells

Drifting in the foam

Little soldier boy

Come marching home

Brave soldier boy

Comes marching home.
