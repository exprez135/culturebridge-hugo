---
title: Executive Summary
bookFlatSection: false
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: "10"
---
# CultureBridge Tulsa

*Creating empathy. Connecting communities.*

## Our Mission

Through participation in an interactive simulation, we promote a greater understanding of the immigrant experience to inspire the people of Tulsa to empathize with immigrants and to unite against cultural and political barriers.

### What is CultureBridge?

Founded by local high school students of diverse backgrounds, CultureBridge offers a brand-new perspective on immigrant life in the United States of America. As our team of 28 students came together, we recognized a lack of empathy for the immigrant experience amongst non-immigrant communities. How could we bridge this gap of understanding? After collaborating with the New Tulsans Initiative, communicating with community organizations, and researching best practices, we created the CultureBridge Immigrant Simulation. By taking on the role of an immigrant in contemporary America, participants have the opportunity to experience both the joys and the hardships of immigrating to and living in our country.

### Why Does Our Community Need CultureBridge?

Now more than ever, our community and our nation can benefit from an organization like CultureBridge, which shatters barriers, social stigmas, and perceptions on the immigrant population. By fostering empathy for the immigrant population, participants are given the tools to take action against polarization and instead, promote unity.

### How Does CultureBridge Stand Out?

CultureBridge Tulsa is unique in that participants are able to step into the stories of real immigrants. Participants become witnesses to not only the process of becoming an immigrant in America but also all of the emotions that it comes with. CultureBridge is tailored to develop empathy for the immigrant community, leading participants toward a more culturally aware and responsible future.

## About the Youth Philanthropy Initiative (YPI)

Youth Philanthropy Initiative equips teens with the necessary tools and understanding to become social entrepreneurs. Teens in YPI are socially and culturally aware, building their projects around issues or problems they believe are pertinent in the Tulsa community. All YPI projects are built to be sustainable through research, organization, and long-term planning, giving students hands-on experiences to use in college and beyond. YPI students are involved in leadership, athletics, speech and debate, art, programming, but they all come together with the same passion to make tangible change in their communities.