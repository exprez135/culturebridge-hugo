---
title: PR Packet
bookFlatSection: false
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: "10"
---
# Our Mission

Through participation in an interactive simulation, we promote a greater understanding of the immigrant experience to inspire the people of Tulsa to empathize with immigrants and to unite against cultural and political barriers.

# What is CultureBridge Tulsa?

Founded by local high school students of diverse backgrounds, CultureBridge offers a brand-new perspective on immigrant life in the United States of America. As our team of 28 students came together, we recognized a lack of empathy for the immigrant experience amongst non-immigrant communities. How could we bridge this gap of understanding? After collaborating with the New Tulsans Initiative, communicating with community organizations, and researching best practices, we created the CultureBridge Immigrant Simulation. By taking on the role of an immigrant in contemporary America, participants have the opportunity to experience both the joys and the hardships of immigrating to and living in our country.

- - -

# The Experience

Taking on the role of a real-life immigrant, participants walk through the story of one of the simulation characters. Along the way, they are tasked with making the tough, self-sacrificing decisions their characters were faced with. To get the most out of this experience, CultureBridge participants are encouraged to actively engage and embrace the reality of each story.

## The Introduction Presentation

The introduction presentation, given by a CultureBridge facilitator, informs the audience of the impact immigrants have locally and nationally. By providing a factual foundation, we hope to better prepare participants to empathize

## The Characters

Each participant is randomly assigned a character and follows their story through the simulation. The initial card participants receive includes a brief background of the character they will be taking on. Because each character’s story is based in reality, we challenge each participant to read the background thoroughly and base their simulation decisions off of the character’s specific circumstances. Countries represented include India, the United Arab Emirates, Mexico, Lebanon, Thailand, Kuwait, Sweden, and Israel.

## The Simulation

As participants follow their character’s story, they forge a unique path as they encounter different events and decisions. Each event leads to a simulated station (e.g. a courthouse or language learning center), where participants might be asked to perform certain tasks. In order to fully grasp an immigrant’s experience, CultureBridge works to mimic different barriers and obstacles of daily life by simulating the unequal treatment and discrimination immigrants face. In addition to displaying the challenges of the immigrant experience, stations are designed to educate viewers about the political and social state of the United States.

## The Debrief

Following the simulation, participants come together to discuss the events, trials, failures, and successes they experienced. The debrief seeds deeper conversation and connection and encourages every participant to turn new found empathy into action.

Immediately after the simulation, participants are asked to complete a survey to reflect on their experience. In addition, we provide an Action Packet with local information to better facilitate future involvement in the immigrant community of Tulsa.

30 days after the simulation, a second check-in survey is distributed to better measure the effectiveness of the CultureBridge simulation.

- - -

# FAQs

### How long does the simulation last?

The entire introduction, simulation, and debrief discussion is approximately one hour and fifteen minutes, however, the length of the simulation is dependent on the number of participants.

### Who participates in the simulation?

CultureBridge Tulsa is open to everyone. No matter your age, profession, or knowledge of immigration, we would love to have you participate in our simulation!

### Does CultureBridge Tulsa push a particular political agenda?

CultureBridge Tulsa does not advocate a particular stance on immigration policy. Our goal is to simply provide a new perspective and have others empathize with the immigrant experience.

### Why does our community need CultureBridge?

Now more than ever, our community and our nation can benefit from an organization like CultureBridge, which shatters barriers, social stigmas, and perceptions on the immigrant population. By fostering empathy for the immigrant population, participants are given the tools to take action against polarization and instead, promote unity.

### How does CultureBridge stand out?

CultureBridge Tulsa is unique in that participants are able to step into the stories of real immigrants. Participants become witnesses to not only the process of becoming an immigrant in America but also all of the emotions that it comes with. CultureBridge is tailored to develop empathy for the immigrant community, leading participants toward a more culturally aware and responsible future.

- - -

# About the Youth Philanthropy Initiative (YPI)

Youth Philanthropy Initiative equips teens with the necessary tools and understanding to become social entrepreneurs. Teens in YPI are socially and culturally aware, building their projects around issues or problems they believe are pertinent in the Tulsa community. All YPI projects are built to be sustainable through research, organization, and long-term planning, giving students hands-on experiences to use in college and beyond. YPI students are involved in leadership, athletics, speech and debate, art, programming, but they all come together with the same passion to make tangible change in their communities.

- - -

## Images

![](https://lh3.googleusercontent.com/XPSTUzMvT8S5GA_txv4Zptf5-Ar7gUaFjeTa8vs0lTbIDebWmJGjH_f5N5V_xsf3CayKZmTIj3QuaJLzlcyqxFEIMJOc1hdW6UTF5J3PScOzvZAQMaHLGKYrSAPZJ6pHvnLUxCW3)![](https://lh3.googleusercontent.com/7YFVv5QSs5WdasuoYysWq42bJLrcBtBKemsVMaUvSMYNHE-AbB9eCirn0i82a4TW0vD12hJJRVn0wcYpLoxLZh3V5tlCL0pkT3t5QQ_uTzoNY_rl5Pyvg2mwYMXF0rk9r5WVNt8S)![](https://lh5.googleusercontent.com/qksDGiB17WO5WqTINVmcasPd8tFNs7mpdtMmD753-th7vbk15LYqjzHkNqb3FCctgGINZwQmvkxMO5V-h4jTgxzvo2xcSyWS7AsEGfcr6AFWtbOA4Lto7pNqXctX6RzDVdYtgjq2)![](https://lh4.googleusercontent.com/HvJYffEY-ic_120o1szRjoyGn9Szw9ROivjKcDO5AJgorlm3tw5OTuktuDLnVUooWe_AtKejEz93LDHAGq1q5gv9GiORvADLIdZt-VNe-peZ_tWIf1rDw2b8pwzdRbgbKcqNGitu)