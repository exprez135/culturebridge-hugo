---
title: Introduction
bookFlatSection: false
bookToc: true
bookHidden: false
bookCollapseSection: false
bookComments: true
weight: 10
---
# Introduction

## POM

**Purpose**: To provide participants with background information pertaining to immigrants in order to foster an understanding of the barriers they encounter and the vital role they play within our communities. The introduction presentation also serves to create an environment in which participants can transition to the simulation.

**Outcomes**:

1. Participants have an understanding of the roles of immigrants in our community.
2. Participants have an introductory level of understanding of the hardships immigrants face.
3. Participants understand the instructions to the simulation and feel fully prepared to begin their journey.

Methods: See below.

- - -

## Before the Presentation

### The Importance of the Introduction

The Introduction to the Presentation and the Simulation is a crucial part of setting the stage and preparing the audience for what is to come. Led by the Facilitator of the Simulation, the Introduction allows for rules, reminders, instructions, and other information to be passed on before beginning the Presentation.

### The Role of the Facilitator

The Facilitator is tasked with the important job of guiding the audience through the Introduction and Presentation to prepare them for the Simulation to come. The Facilitator must ensure that the audience is well-informed of what is to come and is prepared to go through a Simulation that can induce a range of emotions. Being one of the most important jobs, the Facilitator must ensure that the impact is felt among the participants and must guide them through the Introduction, Presentation, and Simulation.

- - -

## The Information Presentation

### What is the Information Presentation and What is its Importance?

The Information Presentation is a collection of information and statistics pertaining to the roles of immigrants and the issues they currently face, which is presented prior to participants undergoing the Simulation. It plays a crucial part in developing a well-informed group of participants to go through the CultureBridge Simulation.

It also serves as a way to engage participants before they go through the Simulation, and this is achieved through interactive questions that will be further discussed in the subsequent section. During this time, participants also begin to develop a bit of empathy through all the information and real stories they are told. They are then able to build upon that empathy as they walk through the Simulation.

- - -

## What is in the Presentation

### Contents

**Slides 1-3:** These slides remind participants to fill out the registration form and contain information about how the project came to be. These slides may be tweaked to also include information about the partner, but it would be greatly appreciated if Cohort 11 of YPI's story remained in this part in some shape or form. (<https://www.culturebridgetulsa.com/>)

**Slide 4:** Cohort 11 of YPI’s mission statement for CultureBridge Tulsa. 

**Slide 5:** The purpose of the information presentation, the outcomes we hope to gain by the end of it, and the methods we will use to achieve those outcomes.

**Slide 6:** A brief overview of the topics that will be covered in the presentation.

**Slide 7:** This slide introduces the story of Pedro, a true story of an immigrant followed throughout the presentation. (<https://www.nytimes.com/2014/06/09/us/for-illegal-immigrants-american-life-lived-in-shadows.html>)

**Slide 8:** This is the first of the question slides which can be found throughout the presentation. (<https://www.americanimmigrationcouncil.org/research/immigrants-in-the-united-states>)

**Slide 9:** A bar chart that depicts the status of U.S immigrants today, including the immigrant population size as well as the regions they have migrated from. (<https://www.americanimmigrationcouncil.org/research/immigrants-in-the-united-states>)

**Slide 10:** Includes the number of documented as well as undocumented immigrants in Oklahoma. (<https://www.americanimmigrationcouncil.org/research/immigrants-in-oklahoma>)

**Slide 11:** This is another question slide. (<https://www.census.gov/newsroom/blogs/random-samplings/2017/03/immigrant_familiesa.html>)

**Slide 12:** This slide contains information about the educational level for immigrants in the United States. The information this represents shows how immigrants compared to native-born citizens usually have a lower level of education other than in the advanced degrees section. (<https://www.census.gov/newsroom/blogs/random-samplings/2017/03/immigrant_familiesa.html>)

**Slide 13:** This slide is similar to the previous slide except it is talking about immigrants from Oklahoma instead of a national scale. (<https://www.americanimmigrationcouncil.org/research/immigrants-in-oklahoma>)

**Slide 14:** This is another question slide like slide 8 and 11. (<https://www.stlouisfed.org/publications/regional-economist/second-quarter-2017/comparing-income-education-and-job-data-for-immigrants-vs-those-born-in-us>)

**Slide 15:** This slide will mainly contain information about the income inequality between immigrants and native-born citizens with immigrants having lower levels of income compared to native-born citizens and how another important information is that immigrant households are typically bigger than native-born immigrants. (<https://www.stlouisfed.org/publications/regional-economist/second-quarter-2017/comparing-income-education-and-job-data-for-immigrants-vs-those-born-in-us>)

**Slide 16:** Research conducted by the New American Economy that highlights the impact immigrants have had on the national economy. (<https://www.newamericaneconomy.org/locations/national/?type&issue>)

**Slide 17:** Research conducted by the New American Economy that highlights the impact immigrants have had on the city of Tulsa’s economy. ([https://research.newamericaneconomy.org/report/new-americans-in-tulsa/](https://www.google.com/url?q=https://research.newamericaneconomy.org/report/new-americans-in-tulsa/&sa=D&ust=1587343624267000))

**Slides 18-20:** These slides serve as an explanation of the Simulation and a transition to it.

**Slide 21:** The follow-up survey link, which should be shown and talked about after the Debrief is completed.

### Importance of the Information

**U.S. and Oklahoma Status on Immigrants**--It is important to understand the number of documented as well as undocumented immigrants in the U.S. It must be further known that not all immigrants are the same and that they migrate from different areas.

**Education and Income**--It is important for the audience to know the educational inequality between foreign-born and native born citizens since their education level ultimately affects their income and consequently their quality of life.

**Economic Impact**--Not many people are aware of how much of the national and local economies is fueled by the work of immigrants. It is important to focus on this topic to emphasize their contributions and to not let them go unnoticed.

**National vs Local Statistics**--While presenting national statistics is important and gives participants almost a bird’s eye view of immigrants’ impact in this country, it can also be difficult for participants to process the high volume of information being given to them. But when participants are presented with statistics that are focused on a specific community they are a part of, they start to become more than just numbers; they become something that participants are tied to. The reality that local immigrants are contributing immensely to their community yet they still face certain barriers begins to set in. The strategy of adding local statistics is aimed to develop the sense of empathy for community immigrants early on in the CultureBridge Tulsa experience.

**Multiple-Choice Questions**--There are three slides of multiple-choice questions spread out throughout the presentation. They are included in order to ensure maximum participation, engagement, and attention of the audience members. They also serve the second purpose of helping the audience truly grasp and remember certain statistics and key information to help them along their journey of truly gaining an understanding of immigrants and learning to empathize with them for the betterment of society.

**Factual Immigrant Story**--A true story of an immigrant, Pedro, is included throughout the presentation. Linking facts and statistics with personal emotions and impacts to truly allow the audience to grasp the consequences of certain situations. This inclusion of the story helps make seemingly inapplicable information to the average citizen become a matter of personal distress. The story helps increase the empathy of the audience towards immigrants as well as preparing them for possible experiences within the simulation.

- - -

## Giving Instructions

On slides 18, 19, and 20 in the information presentation, we have incorporated details to the simulation set-up as well as the instructions. These instructions should be given to the participants who walk through the simulation as immigrants.

On slide 19, pictures of an immigrant’s character cards (Bsquil Antar) are displayed for the audience to see. This is done in order to inform the participants what they should expect to see for their own character. The introductory character card should include language level, country of origin, and background information. The event cards that are further presented on slide 19 include an event that has occurred and two choices that lead to certain outcomes. As explained in the instructions, each decision will lead to a station in which participants obtain the next card and continue their journey. In displaying as well as explaining the
character and event cards of the immigrants, the participants are able
to easily comprehend what to do on their journey.

Slide 20 offers a brief and concise summary of the participants’ instructions: read the character cards, begin the journey, return home, and debrief.

- - -

## After the Presentation

Following the presentation, the participants should read the character cards and begin their journey.