---
title: Surveys
bookFlatSection: false
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: "10"
---
CultureBridge Tulsa: Survey Module

## POM:

**CultureBridge Mission**: Through participation in an interactive simulation, we promote a greater understanding of the immigrant experience to inspire the people of Tulsa to empathize with immigrants and to unite against cultural and political barriers.

**Purpose**: To measure the impact and effectiveness of the simulation.

**Outcomes**: The effectiveness of the simulation is quantifiable.

**Methods**: See below.

## Introduction to the Surveys

### The Importance of Surveys

Surveys ensure that the simulation is efficient. Participants should leave the simulation with more empathy and understanding of the immigrant experience. Surveys provide a concrete method of measuring this empathy and working out any kinks or flaws in the simulation. In addition, survey results allow us to create a network of contacts through which we can distribute information about getting involved in the immigrant community.

## The Survey

### Registration

The first survey is the registration survey which asks questions pertaining to participants' original stance on immigration, previous knowledge of the immigrant experience, and their contact information. This contact information is mandatory as it allows the facilitators to notify participants of the simulation’s logistics and to send more surveys following the simulation. The registration survey should be taken prior to the simulation, either at home or before the introduction.

### The First Follow Up Survey

The first follow up survey should be taken immediately following the simulation. This survey reflects the experience of the simulation itself, helping the facilitators improve the simulation, and measure the immediate effect of the simulation. The survey assesses how well the simulation created empathy, explained an immigrant’s life, and motivated the participants to act.

### One Month Later

Another survey should be sent a month after the simulation to assess the simulation’s long term impact. When the participants took the initial follow-up survey, they were on an emotional high and immigration was on their mind. Over the course of a month, life gets in the way. The One Month Follow Up Survey helps the facilitators know the long term impact and make the necessary changes to the simulation or action packet. This survey also serves as a reminder to the participants about the simulation and how they can help.

## Conclusion of the Surveys

To distribute the surveys, copy the link and send it out to the participants. After the surveys are completed, click the spreadsheet button at the top right corner of the form to access the data. From there, you can view all the participants' responses to analyse and improve the simulation.