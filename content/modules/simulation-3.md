---
title: Simulation
bookFlatSection: true
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: "10"
---
## Simulation POM:

**<span style="text-decoration:underline;">CultureBridge Mission</span>**: Through participation in an interactive simulation, we promote a greater understanding of the immigrant experience to inspire the people of Tulsa to empathize with immigrants and to unite against cultural and political barriers.

**<span style="text-decoration:underline;">Purpose</span>**: To ensure, through reflection, that participants use the lessons of the simulation to empathize with the immigrant community and create positive, lasting action.

**<span style="text-decoration:underline;">Outcomes</span>**: 



1. All misunderstandings about the conduct of the simulation are cleared up.
2. Participants have had the opportunity to focus on serious reflection regarding their experiences and emotions during the simulation.
3. Participants empathise with immigrants in their communities and feel motivated to help them.
4. Participants are given a concrete path forward towards future involvement in their immigrant community. 

**<span style="text-decoration:underline;">Methods</span>**: _See below._



## Introduction to the Simulation

### Starting off

After the intro committee explains the outline of the simulation in the  introduction presentation, the participants will be able to read their first card and begin their journey. Each card has options for the participants. They must choose one location to go to, but if they choose wrong the cards will guide them back to the right direction.  


### The Role of the Station Facilitators {#the-role-of-the-station-facilitators}

The role of the facilitators is to help create a realistic scenario for the participant to feel like they are a real immigrant, to experience the good and the bad. The facilitators hand out the next cards and treat the participants good or bad, depending on their lanyards. Each participants will have a different view of their story, which is good because no two experiences are the same in the real world, either. 


## The Simulation


### Setup

Each station has personal instructions in their designated folder. All stations should put their station sign on the front of their table, and lay out their cards/other resources in the style that is most easily accessible for them in the simulation. More specific  set up details will be entailed in individual station packets which differ for each station. Follow the rest of the instructions carefully to fully be set for the simulation. When you are done, help others set up their stations. When the simulation starts, each facilitator should be seated at their station, quietly and attentively.


### Introduction

After Intro’s presentation, the participants will start, and the facilitators must be in character for their simulation role.


### Going through the Simulation

Use the personal “Simulation” instructions in the station packet information. The instructions include the tips that previous facilitators have carefully reviewed and used successfully that will help you work more efficiently. Please take your role seriously and most authentically to help bring the full affect to the participants.
