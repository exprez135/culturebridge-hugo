---
title: Debrief 
bookFlatSection: false
bookToc: true
bookHidden: false
bookCollapseSection: false
bookComments: true
weight: 20
---
# Debrief Module

\[Cover Page with Logo, pictures of actual Debrief sessions, and more
here\]


Table of Contents:

## POM:

CultureBridge `Mission`: Through participation in an interactive
simulation, we promote a greater understanding of the immigrant
experience to inspire the people of Tulsa to empathize with immigrants
and to unite against cultural and political barriers.

***Purpose***: To ensure, through reflection, that participants use the
lessons of the simulation to empathize with the immigrant community and
create positive, lasting initiative.

***Outcomes***:

1.  All misunderstandings about the *message* of the simulation are
    clarified.
2.  Participants have had the opportunity to focus on serious reflection
    regarding their experiences and emotions during the simulation.
3.  Participants empathize with immigrants in their communities and feel
    motivated to help them.
4.  Participants are given a concrete path forward towards future
    involvement in their immigrant community.

***Methods***: *See below.*


## Introduction to the Debrief

### The Importance of Debriefing

The Debrief is undoubtedly the most important part of the simulation.
This is a time where participants are able to reflect on their
experience and express their emotions throughout the simulation. The
debrief will leave participants with a better outlook on immigrants and
help them better empathize with them for the rest of their lives.

### The Role of the Facilitator

Your role as a facilitator will mainly be focused on guiding the small
group discussion and clearing up any misunderstandings participants had
during the simulation. You are also responsible for helping them fully
understand the importance of the simulation. To start, you should make
the speaking environment as welcoming as possible. You want all of your
participants to be engaged in the discussion and to feel comfortable
with sharing their thoughts. This will yield the most impactful and
productive conversations. As with many discussions, it is inevitable
that it will go off on tangents at times, an imminent one being
politically driven; your job is to bring the group’s focus back to the
simulation. Regardless of what topic your group may stray to, you should
be prepared to quickly divert from it and reiterate how this is a time
for reflection only. After the Debrief, participants should leave with a
fresh perspective on immigrants in Tulsa. Their experience with
CultureBridge Tulsa will be something they will remember for years to
come, so it’s important that the Debrief is effective.

## The Debrief

### Organizing The Participants

Following the end of the simulation, you will split the participants
into small groups for The Debrief. The maximum number of groups you will
have is dependent on the number of facilitators present (e.g. a
simulation run by three facilitators can have a maximum of three small
groups). However, each small group should have at least seven
participants in order to generate the most effective discussions. For
example, if there are fourteen participants and three facilitators, it
would be best to separate into two groups of seven rather than two
groups of five and one group of four.

### How to Organize into Debrief Groups

The Debrief Groups are different from the Home groups in order to allow
people to discuss their character with an intentionally diverse group,
and because many people who finish early already discuss in their Home
groups about their experience.

There are always 5 characters used, but when there are 25 participants
or more, there are 6 characters used. The following chart dictates the
number of break-out debrief groups according to the amount of people
going through the simulation.

17 people or less --------&gt; 3 groups

18+ people --------&gt; 4 groups

The Debriefing groups should be preset before the simulation begins as
soon as the number of participants is known. The facilitators who are
preparing the simulation should take out the appropriate number of name
tags, and assign a debrief group by writing the Group number on the back
of the name tag with a Dry Erase Marker.

It is important to ensure that there are not more than two of the same
character in one debrief group in order to ensure the diversity of
perspective. At the same time, it is extremely important to have two of
the same character (but no more than two) at a debrief table as they
will have different lanyard colors which will contribute to the
discussion of symbolism in the simulation. This provides a good segue
for discussing the purpose of the lanyard colors and how even though
people can have the same situation and choices before them in life, the
judgements and biases that people put on them can seriously affect the
way they go through life.

### Introduction

After separating the participants into different groups, the
facilitators will then introduce themselves. You will thank the
participants for participating in the simulation and afterwards, you
will introduce yourselves to the groups you are facilitating. Once
introductions are done, you can then immediately move forward to the
discussion.

### Discussion

The Debrief discussion is arguably the most important part of the
simulation. During this time, participants are given the freedom to
express their emotions, feelings, and thoughts about what they have
experienced. As a facilitator, you should consider yourself a *guide* as
opposed to an instructor. Below are some guiding questions for how to
develop and encourage discussion. However, the path your Debrief group
follows might be different than what is envisioned below. Refer to the
above section on “The Role of the Facilitator” for best practices in how
to guide conversation, keep on-topic, and avoid contentious issues.

*Please note*: any quoted statements of what to say are here as
examples. Feel free to modify them to better suit your own simulation’s
purpose.

### The What

To start, we will discuss *what* happened. This includes the experiences
they went through, the emotions they are feeling, and their initial
thoughts.

#### Introduction

Start off by saying… “You guys just completed the immigration simulation
and may be feeling many different things. We are going to start the
debrief by \[separating into your small groups and\]<sup>[1]</sup>
discussing your thoughts and emotions.”

#### Emotions

First, ask everyone to go around and say one word that describes their
simulation experience, and why.

Then, connect similar emotions and feelings and ask some respondents why
they said that word. (in big group)

#### The Story and Experience

Ask participants to describe their character, their experience, and
their outcome. Choose participants who went through different characters
to allow other participants to hear different aspects of the immigrant
experience and make them more aware. (in small groups)

#### Connection

After participants share the story they walked through, allow two people
with the same character (but different lanyard color) to compare their
experience. Ask them how they were treated by volunteers at the
stations, and elaborate on any differences. Explain that the color of
the lanyard dictates the manner in which volunteers are supposed to
treat them. Those with blue lanyards experience the worst forms of
racism, rudeness and other microaggressions, along with unnecessarily
long waits at stations. They are essentially deemed as the lowest
priority. The pink lanyards are in the middle ground, and will have some
microaggressions, but on a minor level. Finally, the yellow lanyards
should be treated with respect, and have the opportunity to take pride
and share about their country of origin. The impact of this is that
oftentimes people will make judgments about you based off of something
meaningless like the color of a lanyard.

### The So What

After the discussion of what they just went through, you will then give
the participants reasons why it was important and why it matters.

#### Introduction

Start off by saying “Now that you guys discussed your characters and
your experience, we will discuss why this all matters and why it is
important.”

#### Importance

Ask them questions such as “why does empathy for immigrants matter?”,
“what would it be like without immigrants?”, and “what do you believe
the main reasons why we had this simulation?”

### Conclusion

“So what comes next? How do we think we can help immigrants in our own
communities?”

After this discussion concludes, move directly into the Call to Action.

#### Call to Action

It is important for participants to have the resources in order to help
immigrants. After group discussions are over, you will then give every
participant an action packet filled with organizations in Tulsa which
help immigrants. You will inform the participants of what is the action
packet and what is on there along with the importance of these
organizations saying “These organizations have contributed to the
welfare of immigrants and has put tons of support for the immigrant
community, if you are looking for a way to help immigrants than helping
these organizations would contribute a lot.”

#### The Pitch

The pitch is the section where you can promote your organization and it
does not have a set outline to this. You can discuss this among your
organization and talk about how your social media, goals, and any other
topic you would like the participants to know.

Here is the Link to the Action Packet

[*https://docs.google.com/document/d/1XPfGrKbT4B9HiP5F25DOyQ-GIuzeAAdZRYQr3G-lvFA/edit*](https://docs.google.com/document/d/1XPfGrKbT4B9HiP5F25DOyQ-GIuzeAAdZRYQr3G-lvFA/edit)


## Conclusion of the Simulation

-   Give participants the Initial Participant Follow-up Form via link.

Ideal Time Interval

-   Thirty Minutes total:

<!-- -->

-   First priority is to have all participants discuss their experience
    > in the simulation and have them relate this to their own lives.

-   

Debrief Facilitator Cheat Sheet

[1]  If there are multiple Debrief Groups. See “Organizing the
Participants” above.
